package br.com.andrelino.provacarenet.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BatimentosCardiacoDTO {
	private Long patientId;
	private int value;
	@JsonProperty("timestamp")
	private LocalDateTime datenow;	
}
