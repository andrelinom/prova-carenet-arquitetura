package br.com.andrelino.provacarenet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import br.com.andrelino.provacarenet.domain.BatimentosCardiaco;
import br.com.andrelino.provacarenet.dto.BatimentosCardiacoDTO;
import br.com.andrelino.provacarenet.repository.BatimentosCardiacoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BatimentosCardiacosService {
	
	@Autowired
	private BatimentosCardiacoRepository repository;
	
	public String post(BatimentosCardiacoDTO dto) throws Exception  {
		repository.save(new BatimentosCardiaco(UUIDs.random(), 
												dto.getPatientId(), 
												dto.getValue(), 
												dto.getDatenow())
											   );
		return "Batimento salvo com sucesso!";
	}
	
	public Flux<BatimentosCardiaco> findPatientId(Long patientId) throws Exception {
		return repository.findByPatientId(patientId);
	}
	
}
