package br.com.andrelino.provacarenet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.andrelino.provacarenet.domain.BatimentosCardiaco;
import br.com.andrelino.provacarenet.dto.BatimentosCardiacoDTO;
import br.com.andrelino.provacarenet.service.BatimentosCardiacosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/batimentosCardiaco")
@CrossOrigin
@Api(tags = {"Batimentos Cardiaco."})
public class BatimentosCardiacoController {
	
	@Autowired
	private BatimentosCardiacosService service;

	@GetMapping("/{patientId}")
	@ApiOperation(value = "Busca todos batimentos de um dispositivo.", notes = "Informe o patientId")
	public Flux<BatimentosCardiaco> get(@PathVariable("patientId") Long patientId) {
		try {
			return service.findPatientId(patientId);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Falha em buscar lista de batimentos", e);			
		}
	}
	
	@PostMapping
	@ApiOperation(value = "Salva batimento cardiaco.")
	@ResponseStatus(HttpStatus.CREATED)
	public String post(@RequestBody BatimentosCardiacoDTO dto) {
		try {
			return service.post(dto);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Falha em gravar batimento", e);
		}
	}
	
}
