package br.com.andrelino.provacarenet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvacarenetApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvacarenetApplication.class, args);
	}

}
