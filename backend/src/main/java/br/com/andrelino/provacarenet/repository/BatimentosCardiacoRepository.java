package br.com.andrelino.provacarenet.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

import br.com.andrelino.provacarenet.domain.BatimentosCardiaco;
import reactor.core.publisher.Flux;


public interface BatimentosCardiacoRepository extends ReactiveCassandraRepository<BatimentosCardiaco, UUID>{
	Flux<BatimentosCardiaco> findByPatientId(Long patientId);
}
