package br.com.andrelino.provacarenet.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Table(value = "batimentoscardiaco")
@Getter
@Setter
@AllArgsConstructor
public class BatimentosCardiaco {
	@PrimaryKey
	@JsonIgnore
	private UUID id;
	@Indexed
	private Long patientId;
	private int value;
	@JsonProperty("timestamp")
	private LocalDateTime datenow;	

}
