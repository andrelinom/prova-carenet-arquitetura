package br.com.andrelino.provacarenet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket greetingApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors
						.basePackage("br.com.andrelino.provacarenet.controller"))
						.build()
						.apiInfo(metaData());

	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
					.title("Prova Carenet - Arquitetura")
					.description("Andrelino Martins de Souza - 11 98747-3716")
					.version("1.0.0").build();
	}
}
