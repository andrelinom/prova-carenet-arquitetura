import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiServiceService } from 'src/app/core/api-service.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  searchForm : FormGroup;
  @Input() batimentos;
  inscricao;
  @Input() erro;
  @Input() titulo;
  public paginaAtual = 1;
  constructor(private formBuilder: FormBuilder,
    private apiServiceService : ApiServiceService) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      patientId: ['']
    });
  }
  
  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }

  buscar() {
    console.log("clique");
    const spatientId = this.searchForm.get('patientId').value;
    console.log("request "+spatientId );
    this.inscricao =  this.apiServiceService
    .getBatimentos(spatientId)
    .subscribe(dados => this.batimentos=dados, 
                erro => this.erro=true);   
    console.log(this.batimentos);             
  };

}
