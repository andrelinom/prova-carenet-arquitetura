import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BatimentosComponent } from './batimentos/batimentos.component';
import { ApiServiceService } from './core/api-service.service';
import { ListaComponent } from './batimentos/lista/lista.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,  
    BatimentosComponent,
    ListaComponent    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
